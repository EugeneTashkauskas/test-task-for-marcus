<?php
function import(){
    try {
        $model = new DB();
        $csv = new Reader();
        $logs = array();
        $file_content = $csv->get_content();

        if (empty($file_content)){ throw new Exception('Can\'t read file.'); }
        
        foreach ($file_content as $item){
            // try to add...
            $insert_result = $model->insert($item);

            // if error happens...
            if ($insert_result['result'] !== TRUE){
                // let's add to logs it...
                $logs[] = $insert_result['message'];
            }
        }

        if (empty($logs)){
            // all rows are successfully imported
            return json_encode(array('status' => 'success', 'message' => 'File has been successfully imported.'));
        }

        // import has been finished with errors...
        Logs::add($logs);
        throw new Exception('Import has been finished with errors, Please, check log-file.');

    } catch (Exception $e){
        return json_encode(array('status' => 'error', 'message' => $e->getMessage()));
    }
}