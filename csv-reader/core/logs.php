<?php
class Logs {
    static function add($message = NULL){
        if (empty($message)) { return FALSE; }
        $log_path = Config::base_url . '/' . Config::$log_file;

        $text = date('Y-m-d H:i:s') . ' ----------------------- ' . PHP_EOL;
        $text .= implode('; ' . PHP_EOL, $message);

        file_put_contents($log_path, $text . PHP_EOL , FILE_APPEND);
        return TRUE;
    }
}