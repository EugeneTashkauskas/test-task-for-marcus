<?php

class DB {
    private $db_link = NULL;
    private $db_table = NULL;
    private $db_columns = NULL;
    function __construct() {
        $port = (!empty(Config::$database['port'])) ? ':' . Config::$database['port'] : '';
        $this->db_link = mysql_connect(Config::$database['host'] . $port, Config::$database['login'], Config::$database['password'])
            or die('Can\'t connect to DB-server.');
        mysql_select_db(Config::$database['database'], $this->db_link) or die('Can\'t connect to DB.');
        $this->db_table = Config::$import_options['table_name'];

        // to avoid issues with charsets...
        mysql_query('SET NAMES utf8');
        mysql_query('SET CHARACTER SET utf8');
        mysql_query('SET character_set_client = utf8');
        mysql_query('SET character_set_connection = utf8');
        mysql_query('SET character_set_results = utf8');
    }

    public function insert($data = NULL){
        try {
            // check all required fields...
            if (empty($this->db_table)){ throw new Exception('Please, configure Table field in /core/config.php file.'); }
            if (empty($data)){ throw new Exception('Nothing to insert.'); }

            $values = array_map('mysql_real_escape_string', array_values($data));
            $keys = array_keys($data);

            $query = 'INSERT INTO `' . $this->db_table . '` (`' . implode('`,`', $keys) . '`) VALUES (\'' . implode('\', \'', $values) . '\')';

            $result = mysql_query($query, $this->db_link);
            if (!$result){ throw new Exception('Can\'t insert data. Request: ' . $query . '.'); }

            return(array('status' => 'success', 'message' => 'Data has been successfully added.', 'result' => TRUE));
        } catch(Exception $e) {
            return(array('status' => 'error', 'message' => $e->getMessage(), 'result' => FALSE));
        }
    }


}