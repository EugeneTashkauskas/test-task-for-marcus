<?php
class Reader {
    public function get_content(){
        $file = Config::base_url . '/import/' . Config::$import_options['file_name'];
        // open file...
        if (($file_handler = fopen($file, 'r')) !== FALSE){
            $content = array();
            $row_counter = 0;
            $headers = array();
            $row = array();
            // let's get all rows...
            while (($data = fgetcsv($file_handler, 1000, Config::$csv_options['field_delimeter'], Config::$csv_options['text_delimeter'])) !== FALSE){
                // ...and get all columns...
                for ($column_counter = 0; $column_counter < count($data); $column_counter++){
                    if($row_counter === 0) {
                        // get headers
                        $headers[$column_counter] = $data[$column_counter];
                    } else {
                        // get values
                        $row[$headers[$column_counter]] = $data[$column_counter];
                    }
                }
                if ($row_counter !== 0){
                    // we can try to import this row to DB right now. It will increase performance. But it makes code complicated and not scalable.
                    $content[] = $row;
                }
                $row_counter++;
            }
            fclose($file_handler);
            if (!empty($content)){ return $content; }
        }
        return FALSE;
    }
}