<?php
class Config {
    // 'cause php doesn't support passing variables to the static field, so let's use const...
    const base_url = BASE_URL;
    static $database = array(
        'host' => '127.0.0.1'
        , 'port' => '3306'
        , 'login' => 'root'
        , 'password' => 'root'
        , 'database' => 'csv_import'
    );
    static $import_options = array(
        'file_name' => 'import.csv'
        , 'table_name' => 'content'
    );
    static $csv_options = array(
        'field_delimeter' => ','
        , 'text_delimeter' => '"'
    );
    static $log_file = 'logs.txt';
}
require_once(Config::base_url . '/core/db.php');
require_once(Config::base_url . '/core/reader.php');
require_once(Config::base_url . '/core/logs.php');
require_once(Config::base_url . '/core/import.php');