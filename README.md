# README #

A simple code-example of reading a .CSV file and writing it to a database.

### csv-reader ###

Here is small project which imports .csv files to DB.

File /csv-reader/index.php is an entry-point. 

### Config ###

Open /csv-reader/core/config.php and modify the next parameters if it needs:

* database: host, port, login, password and database name
* import_options: .csv file name and table name in database
* csv_options: delimeters for .csv file (, and " by default)

### Database ###

You can find database dump here: /csv-reader/database.sql

### .CSV File ###

Files for import are locating here: /csv-reader/import/...